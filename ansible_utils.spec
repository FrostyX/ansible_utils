%if ! (0%{?fedora} > 12 || 0%{?rhel} > 5)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}
%endif

Name:           ansible_utils
Version:        0.0.8
Release:        1%{?dist}
Summary:        Utilities for working with ansible playbooks

License:        GPLv3
URL:            https://bitbucket.org/tflink/ansible_utils
Source0:        https://tflink.fedorapeople.org/tools/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

Requires:       ansible
Requires:       python-kitchen
Requires:       PyYAML
BuildRequires:  python2-devel
BuildRequires:  python-setuptools
BuildRequires:  python-kitchen
BuildRequires:  python-mock

# python-dingus isn't built for el7, can't run tests
%if 0%{?rhel} > 6
BuildRequires:  pytest
BuildRequires:  PyYAML
BuildRequires:  python-dingus
BuildRequires:  python-mock
%endif

# the cli uses argparse, which is not part of the standard libaray in python2.6
%if 0%{?rhel} < 7
Requires:       python-argparse
BuildRequires:  python-argparse
%endif

%description
ansible-utils is a set of utilities for working with repositories of ansible
playbooks

%prep
%setup -q


# python-dingus isn't built for el7, can't run tests
%if 0%{?rhel} > 6
%check
%{__python} setup.py test
%endif

%build
%{__python} setup.py build

%install
%{__python} setup.py install --skip-build --root %{buildroot}

mkdir -p %{buildroot}%{_sysconfdir}/ansible_utils
install conf/rbac.yaml.example %{buildroot}%{_sysconfdir}/ansible_utils/rbac.yaml.example

%files
%doc README.rst conf/*
%{python_sitelib}/ansible_utils
%{python_sitelib}/*.egg-info

%attr(755,root,root) %{_bindir}/rbac-playbook
%dir %{_sysconfdir}/ansible_utils
%{_sysconfdir}/ansible_utils/*


%changelog
* Thu Apr 7 2016 Tim Flink <tflink@fedoraproject.org> - 0.0.8-1
- fix traceback so that it works and provides useful information (#2)

* Wed Mar 25 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.6-1
- set working directory before running ansible-playbook

* Wed Mar 25 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.5-1
- changed execution to run through bash so that proper keys would be used

* Thu Mar 19 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.4-2
- fixed url in specfile

* Thu Mar 19 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.4-1
- fixed some small issues in the code

* Wed Mar 18 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.3-2
- fixed specfile for el6, el7 builds

* Wed Mar 18 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.3-1
- small fixes from review
- added python-kitchen, PyYAML as a dependencies

* Mon Jan 26 2015 Tim Flink <tflink@fedoraproject.org> - 0.0.2-1
- changed package name to match repository and module

* Sat Aug 9 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.1-1
- initial packaging
